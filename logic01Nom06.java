package LatihanSendiri;
import java.util.Scanner;
public class logic01Nom06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Deret N : ");
		int deret = input.nextInt();
		
		int[] angka = new int[deret];
		int numberHelper = 1; 
		for(int i = 0; i < angka.length; i++) {
			angka[i] = numberHelper;
			if(i == 2 || i == angka.length -2) {
				System.out.print("* ");
				numberHelper += 4;
			}else {
				System.out.print(angka[i] + " ");
				numberHelper += 4;
			}
		}
	}

}
